﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using TestApplication1WPF.Services;

namespace TestApplication1WPF.Models
{
    class Person
    {
        [CsvIgnore]
        [XmlIgnore]
        public int Id { get; set; }

        public DateTime? Date { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

        public Person()
        {
            Date = null;
        }
    }
}
