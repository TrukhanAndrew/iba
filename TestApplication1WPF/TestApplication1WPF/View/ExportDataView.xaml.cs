﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using TestApplication1WPF.DAL;
using TestApplication1WPF.Models;
using TestApplication1WPF.Services;

namespace TestApplication1WPF.ViewModel
{
    /// <summary>
    /// Логика взаимодействия для ExportDataView.xaml
    /// </summary>
    public partial class ExportDataView : Window
    {
        public List<ComboBox> ListComboBox { get; }
        public List<KeyValuePair<string, List<object>>> ListListsItemsComboBox { get; set; }
        private Dictionary<string, int> _listEndId;
        private Person _entityForReport;
        private PropertyInfo[] _properties;
        private int _countCheckBoxes;

        public ExportDataView() 
        {
            _entityForReport = new Person();
            InitializeComponent();
            ListComboBox = new List<ComboBox>();
            _listEndId = new Dictionary<string, int>();
            _properties = PropertiesForClass.GetProperties(typeof(Person));
            InitializeCheckBoxes();
            ListListsItemsComboBox = SetListListsItemsComboBox<Person>(20);
        }

        private void InitializeCheckBoxes()
        {
             foreach (var p in _properties)
            {
                CheckBox checkBox = new CheckBox();
                checkBox.Content = p.Name;
                checkBox.Name = p.Name;
                ++_countCheckBoxes;
                StackPanelCheckFields.Children.Add(checkBox);
            }
        }

        private List<KeyValuePair<string, List<object>>> SetListListsItemsComboBox<T>(int countElementForDownload)
        {
            if (_properties != null)
            {
                var listListsItemsCombo = new List<KeyValuePair<string, List<object>>>();
                using (DataBaseContext dbctx = new DataBaseContext())
                {
                    for (var i = 0; i < _properties.Length; i++)
                    {

                        var listFromDataBase = dbctx.Persons
                                                    .Take(countElementForDownload)                                                    
                                                    .ToList();

                        _listEndId.Add(_properties[i].Name, listFromDataBase.Last().Id);

                        var list = listFromDataBase.Select((x) => _properties[i].GetValue(x))
                                                   .Distinct()
                                                   .ToList();
                        var keyVP = new KeyValuePair<string, List<object>>(_properties[i].Name, list);
                        listListsItemsCombo.Add(keyVP);
                    }
                }
                return listListsItemsCombo;
            }
            return null;
        }

        private void Combo_DropDownOpened(object sender, EventArgs e)
        {
            var combo = (ComboBox)sender;
            var scrollComboBox = combo.Template.FindName("DropDownScrollViewer", combo) as ScrollViewer;
            scrollComboBox.ScrollChanged += OnScrollChanged;
            combo.DropDownOpened -= Combo_DropDownOpened;
        }

        private void OnScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var scrollViewer = (ScrollViewer)sender;
            ComboBox currentComboBox = (ComboBox)scrollViewer.TemplatedParent;
            if (scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
            {
                RefreshDataDropDownList(currentComboBox);
            }
            currentComboBox.SelectedIndex = (int)e.VerticalOffset;
        }

        private void Combo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
            {
                return;
            }

            string nameField = ((ComboBox)sender)?.Name;
            if (!string.IsNullOrEmpty(nameField))
            {
                PropertyInfo property = _entityForReport.GetType().GetProperty(nameField);
                if (e.AddedItems[0].ToString() == "All")
                {
                    property.SetValue(_entityForReport, property.GetValue(new Person()));
                }
                else
                {
                    property.SetValue(_entityForReport, e.AddedItems[0]);
                }
                LabelEntityForReport.Content = _entityForReport;
            }
        }

        private void RefreshDataDropDownList(ComboBox currentComboBox)
        {
            var changeableList = ListListsItemsComboBox.Where((l) => l.Key == currentComboBox.Name).First();
            DownloadDataForListComboBox(changeableList, 20);
            SetItemsComboBox(currentComboBox);        
        }

        private void DownloadDataForListComboBox(KeyValuePair<string, List<object>> val, int countElementForDownload)
        {
            List<object> list = new List<object>();
            List<object> editableList = val.Value;            
            string nameField = val.Key;
            using (DataBaseContext dbctx = new DataBaseContext())
            {
                var endId = _listEndId[nameField];
                var listFromDataBase = dbctx.Persons
                                            .Where((p) => p.Id > endId && p.Id <= endId + countElementForDownload )                                            
                                            .ToList();
                if (listFromDataBase.Count > 0)
                {
                    _listEndId[nameField] = listFromDataBase.Last().Id;
                    list = listFromDataBase.Select((p) => p.GetType().GetProperty(nameField).GetValue(p))
                                           .Distinct()
                                           .ToList();
                }
            }
            if (list.Count > 0)
            {
                list.AddRange(editableList);
                var tempList = list.Distinct().ToList().OrderBy((l) => l);
                editableList.Clear();
                editableList.AddRange(tempList);
            }
        }

        private void SetItemsComboBox(ComboBox currentComboBox)
        {
            var listItems = ListListsItemsComboBox.Where((l) => l.Key == currentComboBox.Name)
                                                              .First()
                                                              .Value;
            currentComboBox.Items.Clear();
            currentComboBox.Items.Add("All");
            foreach (var i in listItems)
            {
                currentComboBox.Items.Add(i);
            }
        }

        public void OnClickButtonOk(object sender, RoutedEventArgs e)
        {
            int countNotSelectedCheckBox = 0;
            foreach (var child in StackPanelCheckFields.Children)
            {
                CheckBox checkBox = child as CheckBox;
                if (checkBox != null && checkBox.IsChecked == false)
                {
                    ++countNotSelectedCheckBox;
                    var list = ListComboBox.Where((combo) => combo.Name == checkBox.Name);
                    if (list.Count() > 0)
                    {
                        var comboBox = list.First();
                        comboBox.SelectedIndex = 0;
                    }
                }
            }

            LabelIsAnyFieldSelected.Content = !(_countCheckBoxes == countNotSelectedCheckBox);
            StackPanelComboBoxes.Children.Clear();
            CreateComboBoxes();
        }

        private void CreateComboBoxes()
        {
            foreach (var child in StackPanelCheckFields.Children)
            {
                CheckBox checkBox = child as CheckBox;
                if (checkBox != null && checkBox.IsChecked == true)
                {
                    Label label = new Label();
                    label.Content = checkBox.Content;
                    ComboBox comboBox = new ComboBox();
                    comboBox.Name = checkBox.Content.ToString();

                    var list = ListComboBox.Where((combo) => combo.Name == comboBox.Name);

                    if (list.Count() > 0)
                    {
                        comboBox = list.First();
                    }
                    else
                    {
                        SetItemsComboBox(comboBox);
                        comboBox.SelectedIndex = 0;
                        comboBox.MaxDropDownHeight = 100;
                        comboBox.DropDownOpened += Combo_DropDownOpened;
                        comboBox.SelectionChanged += Combo_SelectionChanged;

                        ListComboBox.Add(comboBox);
                    }

                    StackPanelComboBoxes.Children.Add(label);
                    StackPanelComboBoxes.Children.Add(comboBox);

                }
            }
        }

        private void SelectAllButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (var child in StackPanelCheckFields.Children)
            {
                CheckBox checkBox = child as CheckBox;
                if (checkBox != null)
                {
                    checkBox.IsChecked = true;
                }
            }
        }

        private void ClearAllButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (var child in StackPanelCheckFields.Children)
            {
                CheckBox checkBox = child as CheckBox;
                if (checkBox != null)
                {
                    checkBox.IsChecked = false;
                }
            }
        }
    }
}
