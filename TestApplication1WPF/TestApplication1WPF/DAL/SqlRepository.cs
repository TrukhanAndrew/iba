﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TestApplication1WPF.Services;

namespace TestApplication1WPF.DAL
{
    class SqlRepository<T> where T:class
    {
        private readonly string _connectionString;
        private readonly string _nameTableFromDataBase;

        public SqlRepository(string nameConnectionFromAppConfig, string nameTableFromDataBase)
        {
            _connectionString = ConfigurationManager.ConnectionStrings[nameConnectionFromAppConfig].ConnectionString;
            _nameTableFromDataBase = nameTableFromDataBase;
        }

        public List<T> Get(T entity)
        {
            PropertyInfo[] properties = PropertiesForClass.GetProperties(typeof(T));
            properties = properties.Where((p) => p.GetValue(entity) != null).ToArray();
            int countProperties = properties.Count();
            List<T> personsFromDataBase = new List<T>();

            using (DataContext db = new DataContext(_connectionString))
            {
                StringBuilder sqlCommand = new StringBuilder();
                if (countProperties == 0)
                {
                    sqlCommand.AppendFormat("SELECT * FROM {0}", _nameTableFromDataBase);
                }
                else
                {
                    for (int i = 0; i < countProperties; i++)
                    {
                        var nameField = properties[i].Name;
                        var valueField = properties[i].GetValue(entity);
                        if (i == 0)
                        {
                            sqlCommand.AppendFormat("SELECT * FROM {0} WHERE {1}='{2}'", _nameTableFromDataBase, nameField, valueField);
                        }
                        else
                        {
                            sqlCommand.AppendFormat("AND {0}='{1}'", nameField, valueField);
                        }
                    }
                }
                personsFromDataBase = (sqlCommand.Length > 0 ? db.ExecuteQuery<T>(sqlCommand.ToString()) : null).ToList();
            }
            return personsFromDataBase;
        }
    }
}
