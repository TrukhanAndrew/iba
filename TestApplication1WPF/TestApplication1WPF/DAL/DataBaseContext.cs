﻿using System.Data.Entity;
using TestApplication1WPF.Models;

namespace TestApplication1WPF.DAL
{
    class DataBaseContext: DbContext
    {
        public DataBaseContext() : base("PersonsDB")
        {
        }

        public DbSet<Person> Persons { get; set; }
    }
}
