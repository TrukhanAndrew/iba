﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApplication1WPF.ViewModel
{
    class ProgressBarViewModel: PropertyChangedBase
    {
        private readonly IWindowManager _wm;
        private bool _close;
        private string _title;

        public bool Close
        {
            get => _close;
            set
            {
                _close = value;
                NotifyOfPropertyChange(() => Close);
            }
        }

        public string Title
        {
            get => _title;
            set
            {
                _title = value;
                NotifyOfPropertyChange(() => Title);
            }
        }

        public ProgressBarViewModel(IWindowManager wm, string title)
        {
            _wm = wm;
            Title = title;
            Close = false;
        }

        public void CloseWindow()
        {
            Close = true;
        }
    }
}
