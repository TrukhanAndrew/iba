﻿using Caliburn.Micro;
using Microsoft.Win32;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using TestApplication1WPF.DAL;
using TestApplication1WPF.Exeptions;
using TestApplication1WPF.Models;
using TestApplication1WPF.Services;

namespace TestApplication1WPF.ViewModel
{
    class MainWindowViewModel: PropertyChangedBase
    {
        private string _fileName;
        private bool _canButtonConvertFileClick;
        private bool _canButtonOpenFileClick;
        private bool _canButtonOpenFieldSelectionWindowClick;
        private readonly IWindowManager _wm;
        

        public string FileName
        {
            get => _fileName;
            set
            {
                _fileName = value;
                NotifyOfPropertyChange(() => FileName);
                NotifyOfPropertyChange(() => CanButtonConvertFileClick);
            }
        }

        public bool CanButtonConvertFileClick
        {
            get
            {
                return _canButtonConvertFileClick; //&& !string.IsNullOrWhiteSpace(_fileName);
            }
            set
            {
                _canButtonConvertFileClick = value;
                NotifyOfPropertyChange(() => CanButtonConvertFileClick);
            }
        }

        public bool CanButtonOpenFileClick
        {
            get
            {
                return _canButtonOpenFileClick;
            }
            set
            {
                _canButtonOpenFileClick = value;
                NotifyOfPropertyChange(() => CanButtonOpenFileClick);
            }

        }

        public bool CanButtonOpenFieldSelectionWindowClick
        {
            get => _canButtonOpenFieldSelectionWindowClick;
            set
            {
                _canButtonOpenFieldSelectionWindowClick = value;
                NotifyOfPropertyChange(() => CanButtonOpenFieldSelectionWindowClick);
            }

        }

        public MainWindowViewModel(IWindowManager wm)
        {
            _wm = wm;
            _canButtonConvertFileClick = false;
            _canButtonOpenFileClick = true;
            _canButtonOpenFieldSelectionWindowClick = false;
        }
        

        public void ButtonOpenFileClick()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "File csv|*.csv";
            if (dialog.ShowDialog() == true)
            {
                FileName = dialog.FileName;
                CanButtonConvertFileClick = true;
            }
        }

        public async void ButtonConvertFileClick()
        {
            ProgressBarViewModel progBar = new ProgressBarViewModel(_wm, "Converting CSV file");

            //var winds = Application.Current.MainWindow;

            CanButtonConvertFileClick = false;
            CanButtonOpenFileClick = false;
            CanButtonOpenFieldSelectionWindowClick = false;
            _wm.ShowWindow(progBar);
            
               bool resultTask = await (Task.Run(() =>
               {
                       CsvSerializer<Person> csvS = new CsvSerializer<Person>();
                       Queue<Person> result = new Queue<Person>();
                       using (DataBaseContext dbctx = new DataBaseContext())
                       { 
                           dbctx.Configuration.AutoDetectChangesEnabled = false;
                           dbctx.Database.ExecuteSqlCommand("TRUNCATE TABLE People");

                           using (var stream = new FileStream(FileName, FileMode.Open, FileAccess.Read))
                           {
                               while (csvS.EndOfStream != true)
                               {
                                   result = csvS.Deserialize(stream, 10);
                                   if (result == null)
                                   {
                                       dbctx.Database.ExecuteSqlCommand("TRUNCATE TABLE People");
                                       return false;
                                   }
                                   dbctx.Persons.AddRange(result);
                                   dbctx.SaveChanges();
                               }
                           }
                       }                     
                   return true;
               }));            
            progBar.CloseWindow();

            if (resultTask)
            {
                MessageBox.Show("The file was successfully read and written to the database!");
                CanButtonOpenFieldSelectionWindowClick = true;
            }
            else
            {
                CanButtonOpenFieldSelectionWindowClick = false;
            }

            CanButtonConvertFileClick = true;
            CanButtonOpenFileClick = true;
        }

        public void ButtonOpenFieldSelectionWindowClick()
        {
            ExportDataViewModel<Person> exportData = new ExportDataViewModel<Person>(_wm, "PersonsDB", "People");
            _wm.ShowDialog(exportData);            
        }

        public void MainWindow_Closing()
        {
            Application.Current.Shutdown();
        }
    }
}
