﻿using Caliburn.Micro;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using TestApplication1WPF.DAL;
using TestApplication1WPF.Models;
using TestApplication1WPF.Services;

namespace TestApplication1WPF.ViewModel
{
    class ExportDataViewModel<T> : PropertyChangedBase where T: class, new()
    {
        private bool _anyFieldSelected;
        private object _labelEntityForReport;
        private IWindowManager _wm;
        private string _nameConnectionFromAppConfig;
        private string _nameTableFromDataBase;

        private string NameConnectionFromAppConfig
        {
            get
            {
                return _nameConnectionFromAppConfig;
            }
            set 
            {
                if (string.IsNullOrEmpty(value))
                {
                    MessageBox.Show("No connection name found in App.config");
                    Application.Current.Shutdown();
                }
                _nameConnectionFromAppConfig = value;
            }
        }

        private string NameTableFromDataBase
        {
            get
            {
                return _nameTableFromDataBase;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    MessageBox.Show("No table name is specified from the database");
                    Application.Current.Shutdown();
                }
                _nameTableFromDataBase = value;
            }
        }



        public bool LabelIsAnyFieldSelected
        {
            get
            {
                return _anyFieldSelected;
            }

            set
            {
                _anyFieldSelected = value;
                NotifyOfPropertyChange(() => CanButtonLoadXMLClick);
                NotifyOfPropertyChange(() => CanButtonLoadExcelClick);
            }
        }

        public object LabelEntityForReport
        {
            get
            {
                return _labelEntityForReport;
            }
            set
            {
                _labelEntityForReport = value;
            }
        }

        public ExportDataViewModel(IWindowManager wm, string nameConnectionFromAppConfig, string nameTableFromDataBase)
        {
            _wm = wm;
            _labelEntityForReport = new T();
            NameConnectionFromAppConfig = nameConnectionFromAppConfig;
            NameTableFromDataBase = nameTableFromDataBase;
        }

        public bool CanButtonLoadXMLClick
        {
            get { return _anyFieldSelected; }
        }

        public bool CanButtonLoadExcelClick
        {
            get { return _anyFieldSelected; }
        }

        public void ButtonLoadXMLClick()
        {
            T entity = _labelEntityForReport as T;
            SqlRepository<T> repo = new SqlRepository<T>(NameConnectionFromAppConfig, NameTableFromDataBase);
            List<T> personsFromDataBase = repo.Get(entity);

            string xmlFileName = ShowSaveXMLFileDialog();
            if (!string.IsNullOrEmpty(xmlFileName))
            {
                SerializerXML<T> serializer = new SerializerXML<T>(entity);
                serializer.DataSetSerialize(xmlFileName, personsFromDataBase);
                var process = Process.Start("Notepad.exe", xmlFileName);
            }
        }

        public void ButtonLoadExcelClick()
        {
            T entity = _labelEntityForReport as T;
            SqlRepository<T> repo = new SqlRepository<T>(NameConnectionFromAppConfig, _nameTableFromDataBase);
            List<T> personsFromDataBase = repo.Get(entity);

            SerializerExcel<T> serializer = new SerializerExcel<T>(entity);
            serializer.Serialize(personsFromDataBase);
        }

        public string ShowSaveXMLFileDialog()
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "File xml|*.xml";
            dialog.Title = "Save XML File";
            if (dialog.ShowDialog() == true)
                return dialog.FileName;
            return null;
        }
    }    
}
