﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TestApplication1WPF.Services
{
    class SerializerXML<T> where T: class
    {
        private XmlSerializer _serializer;
        private readonly T _entity;
        private readonly PropertyInfo[] _properties;

        public SerializerXML(T entity)
        {
            _entity = entity;
            _properties = PropertiesForClass.GetProperties(typeof(T))
                                            //.Where((p) => p.GetValue(entity) != null)
                                            .ToArray();
        }

        public void DataSetSerialize(string fileName, List<T> listEntityFromDataBase)
        {
            _serializer = new XmlSerializer(typeof(DataSet));
            DataSet dataSet = new DataSet();
            DataTable table = new DataTable(typeof(T).Name);
            List<DataColumn> listDataColumns = new List<DataColumn>();
            int countProperty = _properties.Length;

            for (var i = 0; i < countProperty; i++)
            {
                table.Columns.Add(new DataColumn(_properties[i].Name));                
            }
            dataSet.Tables.Add(table);
            DataRow row;            

            foreach (var ent in listEntityFromDataBase)
            {
                row = table.NewRow();
                for (var i = 0; i < countProperty; i++)
                {
                    row[i] = _properties[i].GetValue(ent);
                }
                table.Rows.Add(row);
            }

            using (TextWriter writer = new StreamWriter(fileName))
            {
                _serializer.Serialize(writer, dataSet);
            }
        }
    }
}
