﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using TestApplication1WPF.Exeptions;

namespace TestApplication1WPF.Services
{
    class CsvSerializer<T> where T : class, new()
    {
        private string[] _columns;
        private PropertyInfo[] _properties;
        private StreamReader _sr;
        private TypeConverter[] _converters;

        public char Separator { get; set; }
        public bool EndOfStream { get; private set; }
        

        public CsvSerializer(string separator = ";")
        {
            Separator = Char.Parse(separator);
            var type = typeof(T);
            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance 
                | BindingFlags.GetProperty | BindingFlags.SetProperty);
            _properties = (from a in properties
                           where a.GetCustomAttribute<CsvIgnoreAttribute>() == null
                           select a).ToArray();
            SetConverters();
        }

        private void SetNamesColumns(StreamReader sr)
        {
            try
            {
                _columns = sr.ReadLine().Split(Separator);
            }
            catch (Exception)
            {
                throw new CustomExeption("The CSV File is Invalid.");
            }
        }

        private bool IsFirstLineAsNamesColumns()
        {
            PropertyInfo[] tempList = new PropertyInfo[_columns.Length] ;
            for (var i = 0; i < _columns.Length; i++)
            {
                var column = _columns[i];
                var p = _properties.Where(a => a.Name == column).ToArray();
                if (p.Length != 1)
                {
                    return false;                    
                }
                tempList[i] = p[0];
            }
            _properties = tempList;
            return true;
        }

        private void SetConverters()
        {
            _converters = new TypeConverter[_properties.Length]; 
            for (int i = 0; i < _properties.Length; i++)
            {
                _converters[i] = TypeDescriptor.GetConverter(_properties[i].PropertyType);
            }
        }


        public Queue<T> Deserialize(Stream stream, int countLineForRead)
        {
            if (_sr == null)
            {
                _sr = new StreamReader(stream);
                SetNamesColumns(_sr);
                if (!IsFirstLineAsNamesColumns())
                {
                    MessageBox.Show("The file does not have a header row " +
                            "or the title bar does not correspond to the fields used for deserialization.");
                    return null;
                }
            }

            if (_sr.EndOfStream)
            {
                MessageBox.Show("The end of the file is reached.");
                return null;
            }

            List<string> rows = new List<string>();
            var data = new Queue<T>();

            try
            {
                for (var i = 0; i < countLineForRead; i++)
                {
                    rows.Add(_sr.ReadLine());
                    if (_sr.EndOfStream)
                    {
                        EndOfStream = _sr.EndOfStream;
                        break;
                    }
                }                

                foreach (var row in rows)
                {
                    if (string.IsNullOrWhiteSpace(row))
                    {
                        continue;
                    }

                    var parts = row.Split(Separator);

                    var datum = new T();

                    for (int i = 0; i < parts.Length; i++)
                    {
                        var value = parts[i];
                        var column = _columns[i];
                        var convertedvalue = _converters[i].ConvertFrom(value);
                        _properties[i].SetValue(datum, convertedvalue, null);
                    }
                    data.Enqueue(datum);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("The CSV File is Invalid.");
                return null;
            }
            return data;
        }        
    }
}
