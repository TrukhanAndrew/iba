﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Excel = Microsoft.Office.Interop.Excel;

namespace TestApplication1WPF.Services
{
    class SerializerExcel<T> where T: class
    {
        private readonly T _entity;
        private readonly PropertyInfo[] _properties;

        public SerializerExcel(T entity)
        {
            _entity = entity;
            _properties = PropertiesForClass.GetProperties(typeof(T))
                                            //.Where((p) => p.GetValue(entity) != null)
                                            .ToArray();
        }

        public void Serialize(List<T> entityFromDataBase)
        {
            int countProperty = _properties.Length;
            int countEntity = entityFromDataBase.Count();

            Excel.Application app = new Excel.Application();
            app.Visible = true;
            Excel.Workbook workbook;
            Excel.Worksheet worksheet;

            workbook = app.Workbooks.Add();
            worksheet = (Excel.Worksheet)workbook.Worksheets.get_Item(1);

            for (var j = 0; j < countProperty; j++)
            {
                worksheet.Cells[1, j + 1] = _properties[j].Name;
                var i = 0;
                foreach (var ent in entityFromDataBase)
                {
                    worksheet.Cells[i + 2, j + 1] = _properties[j].GetValue(ent);
                    i++;
                }
            }
        }
    }
}
