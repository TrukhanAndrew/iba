﻿using System;
using System.Linq;
using System.Reflection;

namespace TestApplication1WPF.Services
{
    class PropertiesForClass
    {
        public static PropertyInfo[] GetProperties(Type type)
        {
            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance
                | BindingFlags.GetProperty | BindingFlags.SetProperty);

            properties = (from a in properties
                          where a.GetCustomAttribute<CsvIgnoreAttribute>() == null
                          select a).ToArray();

            return properties;
        }
    }
}
