﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApplication1WPF.Exeptions
{
    class CustomExeption: Exception
    {
        public CustomExeption(string message, Exception ex): base(message, ex)
        {
        }

        public CustomExeption(string message) : base(message)
        {
        }
    }
}
